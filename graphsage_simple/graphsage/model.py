import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn import init
from torch.autograd import Variable

import numpy as np
import time
import random
from sklearn.metrics import f1_score
from collections import defaultdict


from graphsage_simple.graphsage.encoders import Encoder
from graphsage_simple.graphsage.aggregators import MeanAggregator
from graphsage_simple.graphsage.prediction import BipartiteEdgePredLayer
import pdb
import time

"""
Simple supervised GraphSAGE model as well as examples running the model
on the Cora and Pubmed datasets.
"""
def fixed_unigram_candidate_sampler(num_sampled, unique, range_max, distortion, unigrams):
    weights = unigrams**distortion
    prob = weights/weights.sum()
    sampled = np.random.choice(range_max, num_sampled, p=prob, replace=~unique)
    return sampled

class SupervisedGraphSage(nn.Module):

    def __init__(self, degrees, adj_lists, feat_data,args):
        super(SupervisedGraphSage, self).__init__()
        self.args = args
        self.adj_lists = adj_lists
        self.degrees = degrees
        
        self.feat_data = Variable(torch.FloatTensor(feat_data), requires_grad = False)

        self.linear1 = nn.Linear(2*self.feat_data.shape[1], self.feat_data.shape[1])
        self.tanh = nn.Tanh()
        self.linear2 = nn.Linear(2*self.feat_data.shape[1], self.args.dim_2)        

        self.neg_sample_size = 10
       
        self.normalize_embedding = True
        self.link_pred_layer = BipartiteEdgePredLayer(is_normalized_input=self.normalize_embedding)

    def create_neib_matrix(self, adj_list):
        max_degree = max(self.degrees)
        neib_matrix = np.zeros((len(adj_list), max_degree))
        neib_matrix += len(adj_list)
        for i in range(len(neib_matrix)):
            neib_matrix[i, :len(adj_list[i])] = np.array(list(adj_list[i]))
        self.degrees_torch = torch.FloatTensor(self.degrees)
        if self.args.cuda:
            self.degrees_torch = self.degrees_torch.cuda()
        return torch.LongTensor(neib_matrix)

    
    def aggregator(self, nodes): 
        timem = time.time()
        if self.args.cuda:
            self.feat_data = self.feat_data.cuda()
        init_nodes = nodes
        means2 = torch.zeros(len(nodes), self.feat_data.shape[1])

        to_cat = torch.zeros((1, self.feat_data.shape[1]))
        if self.args.cuda:
            to_cat = to_cat.cuda()
        self.feat_data = torch.cat([self.feat_data, to_cat], dim=0)
        for node in init_nodes:
            nodes = set(nodes).union(self.adj_lists[node])

        means1 = torch.zeros(len(nodes), self.feat_data.shape[1])
        if self.args.cuda:
            means1 = means1.cuda()
            means2 = means2.cuda()

        new_id2idx = {node:i for i,node in enumerate(nodes)}
        for node in nodes:
            mean1 = self.feat_data[list(self.adj_lists[node])].mean(dim=0)
            means1[new_id2idx[node]] = mean1
        emb_hop1 = torch.cat((self.feat_data[list(nodes)], means1), dim=1)

        emb_hop1 = self.linear1(emb_hop1)
        emb_hop1 = self.tanh(emb_hop1)

        for i,node in enumerate(init_nodes):
            mean2 = emb_hop1[[new_id2idx[ele] for ele in self.adj_lists[node]]].mean(dim=0)
            means2[i] = mean2
        emb_hop2 = torch.cat((self.feat_data[list(init_nodes)], means2), dim=1)
        emb_hop = self.linear2(emb_hop2)
        return emb_hop

    def forward(self, inputs1, inputs2):        
        neg = fixed_unigram_candidate_sampler(
            num_sampled=self.neg_sample_size,
            unique=False,
            range_max=len(self.degrees),
            distortion=0.75,
            unigrams=self.degrees
        )
        outputs11 = self.aggregator(inputs1.tolist())
        outputs21 = self.aggregator(inputs2.tolist())
        neg_outputs = self.aggregator(neg.tolist())

        if self.normalize_embedding:
            outputs1 = F.normalize(outputs11, dim=1)
            outputs2 = F.normalize(outputs21, dim=1)
            neg_outputs = F.normalize(neg_outputs, dim=1)
           
        return outputs1, outputs2, neg_outputs

    def loss(self, inputs1, inputs2):
        batch_size = inputs1.size()[0]
        outputs1, outputs2, neg_outputs  = self.forward(inputs1, inputs2)        
        loss = self.link_pred_layer.loss(outputs1, outputs2, neg_outputs) / batch_size
        return loss


def run_graph(graph_data,args):
    batch_size = args.batch_size
    np.random.seed(1)
    random.seed(1)

    adj_lists = graph_data.full_adj
    feat_data = graph_data.raw_feats
    all_edges = graph_data.all_edges

    graphsage = SupervisedGraphSage(graph_data.deg, adj_lists, feat_data, args)
    if args.cuda:
        graphsage.cuda()
    optimizer = torch.optim.Adam(filter(lambda p : p.requires_grad, graphsage.parameters()), lr=args.learning_rate)

    n_iters = len(all_edges)//batch_size
     
    for epoch in range(args.epochs):
        print("Epoch {0}".format(epoch))
        np.random.shuffle(all_edges)
        for iter in range(n_iters):
            batch_edges = torch.LongTensor(all_edges[iter*batch_size:(iter+1)*batch_size])
            optimizer.zero_grad()
            loss = graphsage.loss(batch_edges[:,0], batch_edges[:,1])
            loss.backward()
            optimizer.step()
            if iter % 4 == 0:
                print(loss)
    embeddings = F.normalize(graphsage.aggregator(list(range(feat_data.shape[0]))), dim = 1)

    embeddings = embeddings.detach().cpu().numpy()
    return embeddings, graphsage

        
